package com.securelocation.apps.locationremainder.ui.activities;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Toast;

import com.securelocation.apps.locationremainder.R;


/**
 * The Class BaseActivity.
 */
public abstract class BaseActivity extends AppCompatActivity {


    /**
     * The m progress dialog.
     */
    private static ProgressDialog mProgressDialog;

    /**
     * The Constant ADD_FRAGMENT.
     */
    public static final int ADD_FRAGMENT = 0;

    /**
     * The Constant REPLACE_FRAGMENT.
     */
    public static final int REPLACE_FRAGMENT = 1;

    /**
     * The Constant DEFAULT.
     */
    public static final int DEFAULT = 001;

    // ui
    private Toolbar mActionBarToolbar;

    /* (non-Javadoc)
     * @see android.support.v4.app.FragmentActivity#onCreate(android.os.Bundle)
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        overridePendingTransition(R.anim.pull_in_right, R.anim.push_out_left);
        getActionBarToolbar();
        ActionBar ab = getSupportActionBar();
        if (ab != null) {
            ab.setDisplayHomeAsUpEnabled(true);
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        initializeComponents();
        setListeners();
    }

    /* (non-Javadoc)
             * @see android.support.v4.app.FragmentActivity#onResume()
             */
    @Override
    protected void onResume() {
        super.onResume();
    }

    /* (non-Javadoc)
     * @see android.support.v4.app.FragmentActivity#onPause()
     */
    @Override
    protected void onPause() {
        super.onPause();
    }


    /**
     * Initialize components.
     */
    protected abstract void initializeComponents();

    /**
     * Sets the listeners.
     */
    protected abstract void setListeners();


    /**
     * Short toast.
     *
     * @param msg the msg
     */
    public void shortToast(String msg) {
        Toast.makeText(BaseActivity.this, msg, Toast.LENGTH_SHORT).show();
    }

    /**
     * Long toast.
     *
     * @param msg the msg
     */
    public void longToast(String msg) {
        Toast.makeText(BaseActivity.this, msg, Toast.LENGTH_LONG).show();
    }

    /**
     * Show progress dialog.
     *
     * @param context the context
     * @param msg     the msg
     */
    public static void showProgressDialog(Context context, String msg) {
        mProgressDialog = new ProgressDialog(context);
        mProgressDialog.setMessage(msg);
        mProgressDialog.setCancelable(true);
        mProgressDialog.show();
    }

    /**
     * Sets the message progress.
     *
     * @param msg the new message progress
     */
    public static void setMessageProgress(String msg) {
        if (mProgressDialog != null)
            mProgressDialog.setMessage(msg);
    }

    /**
     * Close progress dialog.
     */
    public static void closeProgressDialog() {
        if (mProgressDialog != null && mProgressDialog.isShowing()) {
            mProgressDialog.dismiss();
            mProgressDialog = null;
        }
    }

    /**
     * Fragment transaction.
     *
     * @param container        the container
     * @param transactionType  the transaction type
     * @param fragment         the fragment
     * @param isAddToBackStack the is add to back stack
     * @param tag              the tag
     */
    public void fragmentTransaction(int container, int transactionType,
                                    Fragment fragment, boolean isAddToBackStack, String tag) {

        FragmentTransaction trans = getSupportFragmentManager()
                .beginTransaction();
        switch (transactionType) {
            case (ADD_FRAGMENT):

                trans.add(container, fragment, tag);
                break;
            case (REPLACE_FRAGMENT):

                trans.replace(container, fragment, tag);
                if (isAddToBackStack)
                    trans.addToBackStack(null);

                break;

        }
        trans.commit();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.pull_in_right, R.anim.push_out_left);
    }

    /**
     * Show toast.
     *
     * @param msg the msg
     */
    protected void showToast(String msg) {
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
    }

    /**
     * Show toast.
     *
     * @param msg the msg
     */
    protected void showToast(int msgId) {
        Toast.makeText(this, msgId, Toast.LENGTH_SHORT).show();
    }

    protected Toolbar getActionBarToolbar() {
        if (mActionBarToolbar == null) {
//            mActionBarToolbar = (Toolbar) findViewById(R.id.toolbar_actionbar);
            if (mActionBarToolbar != null) {
                setSupportActionBar(mActionBarToolbar);
            }
        }
        return mActionBarToolbar;
    }

    public void setupToolbar(){
        getActionBarToolbar();
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    public void setTitle(String title) {
        /*TextView lblTitle = (TextView) getActionBarToolbar().findViewById(R.id.toolbar_title);
        lblTitle.setText(title);*/
        getSupportActionBar().setTitle(title);
    }

    public void setTitle(int id) {
        /*TextView lblTitle = (TextView) getActionBarToolbar().findViewById(R.id.toolbar_title);
        lblTitle.setText(title);*/
        getSupportActionBar().setTitle(id);
    }

    public static final void showSnackbar(View parentLayout, int stringTextId, boolean isLong){
        if(isLong) {
            Snackbar
                    .make(parentLayout, stringTextId, Snackbar.LENGTH_LONG)
                    .show();
        }else {
            Snackbar
                    .make(parentLayout, stringTextId, Snackbar.LENGTH_SHORT)
                    .show();
        }
    }

    public static final void showSnackbar(View parentLayout, String msg, boolean isLong){
        if(isLong) {
            Snackbar
                    .make(parentLayout, msg, Snackbar.LENGTH_LONG)
                    .show();
        }else {
            Snackbar
                    .make(parentLayout, msg, Snackbar.LENGTH_SHORT)
                    .show();
        }
    }

}
